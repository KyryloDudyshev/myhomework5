package myhomework5.myhomework5Test;

import myhomework5.myhomework5Classes.Human;
import myhomework5.myhomework5Classes.Pet;
import org.junit.jupiter.api.Test;

import java.util.Arrays;

import static org.junit.jupiter.api.Assertions.*;

class HumanTest {
    Human john = new Human("John","Wick",1980);
    Human human = new Human();

    @Test
    void testToString() {
        String actual = john.toString();
        String expected = "Human {name = " + john.getName() + ", " + "surname = " + john.getSurname() + ", " + "year = " + john.getYear() + ", "  + "iq = " + john.getIq() + ", " + Arrays.deepToString(john.getSchedule()) + "}";
        assertEquals(expected,actual);
    }

    @Test
    void testHashcode(){
        assertTrue(human.hashCode()!=john.hashCode());
    }

    @Test
    void testEqualsFalse(){
        assertFalse(john.equals(human));
    }

    @Test
    void testEqualsTrue(){
        assertTrue(john.equals(john));
    }

    //contract Equals
    @Test
    void testSymmetry() {
        assertEquals(human.equals(john),john.equals(human));
    }

    @Test
    void testTransitivity() {
        Human human2 = new Human();
        boolean[] test = {john.equals(human2),human.equals(human2),john.equals(human2)};
        assertEquals(Arrays.toString(new boolean[]{false,false,false}),Arrays.toString(test));
    }

    @Test
    void testConsistency() {
        boolean firstInvoke = john.equals(human);
        boolean secondInvoke = john.equals(human);
        assertEquals(firstInvoke,secondInvoke);
    }

    @Test
    void testNull() {
        assertEquals(false,john.equals(null));
    }
}