package myhomework5;

import myhomework5.myhomework5Classes.Family;
import myhomework5.myhomework5Classes.Human;
import myhomework5.myhomework5Classes.Pet;
import myhomework5.myhomework5Enums.DayOfWeek;
import myhomework5.myhomework5Enums.Species;

import java.util.Arrays;

public class Main {
    public static void main(String[] args) {
        //first family
        Human john = new Human("John","Wick",1980);
        john.setIq(150);
        String[][] scheduleJohn = new String[2][2];
        scheduleJohn[0][0] = DayOfWeek.SATURDAY.name();
        scheduleJohn[0][1] = "Play football";
        scheduleJohn[1][0] = DayOfWeek.SUNDAY.name();
        scheduleJohn[1][1] = "Goto the gym";
        john.setSchedule(scheduleJohn);
        Human karla = new Human("Karla","Bruni",1980);
        karla.setIq(150);
        String[][] scheduleKarla = new String[2][2];
        scheduleKarla[0][0] = DayOfWeek.SATURDAY.name();
        scheduleKarla[0][1] = "Go shopping";
        scheduleKarla[1][0] = DayOfWeek.SUNDAY.name();
        scheduleKarla[1][1] = "Play tennis";
        karla.setSchedule(scheduleKarla);
        Family familyWick = new Family(karla, john);
        Pet dog = new Pet(Species.DOG,"Alfa",3,95, new String[]{"play","go for a walk"});
        dog.getSpecies().setNumberOfPaw(4);
        dog.getSpecies().setHasFur(true);
        dog.getSpecies().setCanFly(false);
        familyWick.setPet(dog);
        Human natali = new Human("Natali","Portman",1990,97,scheduleKarla);
        familyWick.addChild(natali);
        natali.setFamily(familyWick);
        Human liza = new Human("Liza","Mineli",2008,109,scheduleJohn);
        familyWick.addChild(liza);
        liza.setFamily(familyWick);

        //second family
        Human arnold = new Human("Arnold","Schwarzenegger",1960);
        arnold.setIq(150);
        String[][] scheduleArnold = new String[2][2];
        scheduleArnold[0][0] = DayOfWeek.SATURDAY.name();
        scheduleArnold[0][1] = "Go to the gym";
        scheduleArnold[1][0] = DayOfWeek.SUNDAY.name();
        scheduleArnold[1][1] = "Go to the gym";
        arnold.setSchedule(scheduleArnold);
        Human monica = new Human("Monica","Belucci",1962);
        monica.setIq(150);
        String[][] scheduleMonica = new String[2][2];
        scheduleMonica[0][0] = DayOfWeek.SATURDAY.name();
        scheduleMonica[0][1] = "Visit Jennifer";
        scheduleMonica[1][0] = DayOfWeek.SUNDAY.name();
        scheduleMonica[1][1] = "Go to the bar";
        monica.setSchedule(scheduleMonica);
        Family familyArnold = new Family(arnold, monica);
        Pet cat = new Pet(Species.CAT,"Olaf",4,50, new String[]{"sleep","eat"});
        cat.getSpecies().setCanFly(false);
        cat.getSpecies().setHasFur(true);
        cat .getSpecies().setNumberOfPaw(4);
        familyArnold.setPet(cat);
        Human michel = new Human("Michel","Schwarzeneger",1992,37,scheduleMonica);
        familyArnold.addChild(michel);
        michel.setFamily(familyArnold);

        System.out.println(familyArnold);
        System.out.println(familyWick);

        //вызов методов ребёнка second family
        System.out.println(michel);
        michel.describePet();
        michel.greetPet();
        michel.feedPet(false);

        //вызов методов Pet
        System.out.println(cat);
        cat.eat();
        cat.respond();
        cat.foul();

        //вызов метода delete child через human
        System.out.println(Arrays.toString(familyWick.getChildren()));
        familyWick.deleteChild(liza);
        System.out.println(Arrays.toString(familyWick.getChildren()));
        familyArnold.deleteChild(liza);


//        int count = 0;
//        while (count < 10000000) {
//            Human human = new Human();
//        }


    }



}
