package myhomework5.myhomework5Classes;

import myhomework5.myhomework5Enums.Species;

import java.util.Arrays;

public class Pet {
    private Species species;
    private String nickname;
    private int age;
    private int trickLevel;
    String[] habits;

    public Pet(Species species, String nickname, int age, int trickLevel, String[] habits) {
        this.species = species;
        this.nickname = nickname;
        this.age = age;
        this.trickLevel = trickLevel;
        this.habits = habits;
    }

    public Pet(Species species, String nickname) {
        this.species = species;
        this.nickname = nickname;
    }

    public Pet() {
    }

    public Species getSpecies() {
        return species;
    }

    public void setSpecies(Species species) {
        this.species = species;
    }

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public int getTrickLevel() {
        return trickLevel;
    }

    public void setTrickLevel(int trickLevel) {
        if (trickLevel >= 0 & trickLevel <= 100) {
            this.trickLevel = trickLevel;
        } else {
            System.out.println("incorrect level of trick");
        }
    }

    public String[] getHabits() {
        return habits;
    }

    public void setHabits(String[] habits) {
        this.habits = habits;
    }

    @Override
    public int hashCode() {
        return super.hashCode();
    }

    @Override
    public boolean equals(Object obj) {
        return super.equals(obj);
    }

    @Override
    public String toString() {
        return this.getSpecies() + " nickname = " + this.getNickname() + "," + " age = " + this.getAge() + "," + " trickLevel = " + this.getTrickLevel() + "," + " habits = " + Arrays.toString(this.getHabits()) + ", can fly " + this.getSpecies().isCanFly() + ", has fur " + this.getSpecies().isHasFur() + ", number of paw " + this.getSpecies().getNumberOfPaw();
    }

    @Override
    protected void finalize(){
        System.out.println("Object Pet deleted");
    }

    public void eat() {
        System.out.println("Я кушаю!");
    }

    public void respond() {
        System.out.println("Привет, хозяин. Я - " + this.nickname + "." + " Я соскучился!");
    }

    public void foul() {
        System.out.println("Нужно хорошо замести следы...");
    }




}
