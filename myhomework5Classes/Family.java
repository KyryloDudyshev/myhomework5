package myhomework5.myhomework5Classes;

import java.util.Arrays;

public class Family {
    private Human mother;
    private Human father;
    private Human[] children;
    private Pet pet;

    public Family(Human mother, Human father) {
        this.mother = mother;
        this.father = father;
        this.children = new Human[0];
    }

    public Human getMother() {
        return mother;
    }

    public void setMother(Human mother) {
        this.mother = mother;
    }

    public Human getFather() {
        return father;
    }

    public void setFather(Human father) {
        this.father = father;
    }

    public Human[] getChildren() {
        return children;
    }

    public void setChildren(Human[] children) {
        this.children = children;
    }

    public Pet getPet() {
        return pet;
    }

    public void setPet(Pet pet) {
        this.pet = pet;
    }

    @Override
    public String toString() {
        return "Family: mother: " + mother.getName() + " " + mother.getSurname() + ", " + "father: " + father.getName() + " " + father.getSurname() + ", children: " + Arrays.toString(children);
    }

    @Override
    protected void finalize(){
        System.out.println("Object Family deleted");
    }

    @Override
    public int hashCode() {
        return super.hashCode();
    }

    @Override
    public boolean equals(Object obj) {
        return super.equals(obj);
    }

    public void addChild(Human human) {
        Human[] childrenBorn = Arrays.copyOf(children, children.length + 1);
        childrenBorn[childrenBorn.length - 1] = human;
        children = new Human[childrenBorn.length];
        children = childrenBorn.clone();
    }

    public boolean deleteChild(int index) {
        if (children.length == 0) {
            System.out.println("Incorrect index");
            return false;
        } else if (index > children.length | index < 0) {
            System.out.println("Incorrect index");
            return false;
        } else {
            if (children.length - 1 - index >= 0)
                System.arraycopy(children, index + 1, children, index, children.length - 1 - index);
            Human[] childrenDelete = Arrays.copyOf(children, children.length - 1);
            children = new Human[childrenDelete.length];
            children = childrenDelete.clone();
            return true;
        }
    }

    public void deleteChild(Human human) {
        int index = 0;
        for (Human child : children) {
            if (child.getName().equals(human.getName())) {
                index++;
            }
        }
        if (index == 0) {
            System.out.println("No child with this name in this family");
        }
        if (children.length == 0) {
            System.out.println("There is no child in this family");
        } else {
            for (int i = 0; i < children.length; i++) {
                if ((children[i].hashCode() == human.hashCode()) && (children[i].getName().equals(human.getName()))) {
                    Human[] childrenDelete = children.clone();
                    childrenDelete[i] = childrenDelete[childrenDelete.length - 1];
                    children = Arrays.copyOf(childrenDelete, childrenDelete.length - 1);
                }
            }
        }
    }

    public int countFamily() {
        return 2 + children.length;
    }
}