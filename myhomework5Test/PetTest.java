package myhomework5.myhomework5Test;

import myhomework5.myhomework5Classes.Pet;
import myhomework5.myhomework5Enums.Species;
import org.junit.jupiter.api.Test;

import java.util.Arrays;

import static org.junit.jupiter.api.Assertions.*;

class PetTest {
    Pet pet = new Pet();
    Pet pet2 = new Pet();

    @Test
    void testToString() {
        pet.setSpecies(Species.PARROT);
        String actual = pet.toString();
        String expected = pet.getSpecies() + " nickname = " + pet.getNickname() + "," + " age = " + pet.getAge() + "," + " trickLevel = " + pet.getTrickLevel() + "," + " habits = " + Arrays.toString(pet.getHabits()) + ", can fly " + pet.getSpecies().isCanFly() + ", has fur " + pet.getSpecies().isHasFur() + ", number of paw " + pet.getSpecies().getNumberOfPaw();
        assertEquals(expected,actual);
    }

    @Test
    void testHashcode(){
        assertTrue(pet.hashCode()!=pet2.hashCode());
    }

    @Test
    void testEqualsFalse(){
        assertFalse(pet2.equals(pet));
    }

    @Test
    void testEqualsTrue(){
        assertTrue(pet.equals(pet));
    }

    //contract Equals
    @Test
    void testSymmetry() {
        assertEquals(pet.equals(pet2),pet2.equals(pet));
    }

    @Test
    void testTransitivity() {
        Pet pet3 = new Pet();
        boolean[] test = {pet.equals(pet2),pet2.equals(pet3),pet.equals(pet3)};
        assertEquals(Arrays.toString(new boolean[]{false,false,false}),Arrays.toString(test));
    }

    @Test
    void testConsistency() {
        boolean firstInvoke = pet.equals(pet2);
        boolean secondInvoke = pet.equals(pet2);
        assertEquals(firstInvoke,secondInvoke);
    }

    @Test
    void testNull() {
        assertEquals(false,pet.equals(null));
    }
}